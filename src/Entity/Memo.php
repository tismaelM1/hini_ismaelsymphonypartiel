<?php

namespace App\Entity;

use App\Repository\MemoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MemoRepository::class)
 */
class Memo
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    public $id;

    /**
     * @ORM\Column(type="string")
     */
    public $contenue;

    /**
     * @ORM\Column(type="datetime")
     */
    public $expiration;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContenue()
    {
        return $this->contenue;
    }

    public function setContenue( $contenue): self
    {
        $this->contenue = $contenue;

        return $this;
    }

    public function getExpiration()
    {
        return $this->expiration;
    }

    public function setExpiration( $expiration): self
    {
        $this->expiration = $expiration;

        return $this;
    }
}
