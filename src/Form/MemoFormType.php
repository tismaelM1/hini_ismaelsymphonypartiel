<?php

namespace App\Form;

use App\Entity\Memo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;


class MemoFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('contenue', TextareaType::class,[
                'required' => true,
                'attr' => [
                    'class'=>"form-control"
                ],
            ])
            ->add('expiration', NumberType::class, [
                'label'    => 'durée de vie en minutes ',
                'required' => true,
                'scale'    => 2,
                "html5"    =>true,
                'attr'     => array(
                    'min'  => 1,
                    'max'  => 180,
                    'step' => 1,
                    'class'=>"form-control",
                    "type"=>"number",
                    "value"=> 20
                ),
            ])
            ->add('enregistrer', SubmitType::class, [
                'attr' => ['class' => 'form-control btn btn-outline-secondary'],
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Memo::class,
        ]);
    }
}
