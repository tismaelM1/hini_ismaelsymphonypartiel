<?php

namespace App\Controller;

use App\Form\MemoFormType;
use App\Repository\MemoRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class WelcomeController extends AbstractController
{
    /**
     * @Route("/", name="welcome")
     */
    public function index(MemoRepository $memoRepository): Response
    {
        $memoForm= $this->createForm(MemoFormType::class);

        $memoRepo = $memoRepository->createQueryBuilder('memo');
        $allMemo = $memoRepo->select("memo.contenue","memo.expiration","memo.id")
        ->getQuery()
        // ->execute()
        ->getArrayResult()
        ;
        
        // dd($allMemo);

        return $this->render('welcome/index.html.twig', [
            'controller_name' => 'WelcomeController',
            'memoForm'=>$memoForm->createView(),
            'allMemo'=>$allMemo,


        ]);
    }


    
}
