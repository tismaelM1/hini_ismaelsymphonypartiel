<?php

namespace App\Controller;

use DateTime;
use DateInterval;
use App\Entity\Memo;
use App\Form\MemoFormType;
use App\Repository\MemoRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MemoController extends AbstractController
{
    /**
     * @Route("/memo/{id}", name="memo.show")
     */
    public function show(Memo $memo): Response
    {
        $secondes  = $memo->expiration->getTimestamp()-time();
        $minute =  $secondes/60;
        if($minute >0){
            return $this->render('memo/show.html.twig', [
                'controller_name' => 'MemoController',
                'memo' => $memo,
            ]);
        }
        else {
            $response = new Response();
            $response->headers->set('Content-Type', 'text/html');
            $response->setStatusCode(410);
            return $response;
        }

    }



    /**
     * @Route("/memo_add", name="memo.add",methods={"POST"})
     */
    // #[Route('/gallery_add', name: 'gall.add', methods: ['POST'])]
    public function store(Request $request, EntityManagerInterface $entityManager){
        $form= $this->createForm(MemoFormType::class);

        $memo = new Memo();


        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            // dd('fff');
            // encode the plain password
            $contenue = $form->get('contenue')->getData();
            $creationDate=new DateTime();
            // dd($creationDate);
            $creationDate->add(new DateInterval('PT' . $form->get('expiration')->getData() . 'M'));

            $expiration = $creationDate;

            // dd($expiration);

            $memo->setContenue($contenue);
            $memo->setExpiration($expiration);

            $entityManager->persist($memo);
            $entityManager->flush();

            // dd($memo->id);
            // return $this->redirectToRoute('memo',$memo->id);
            return $this->redirectToRoute('memo.show', array(
                'id' => $memo->id,
        ));
            // return $this->redirect('memo.show', array('year' => 6, 'month' => 6));

        }
        dd('jeje');
    }
}
